# CS371p: Object-Oriented Programming Collatz Repo

* Name: Kiarash Sadr

* EID: ss65729

* GitLab ID: dsynkd

* HackerRank ID: dsynkd

* Git SHA: 652286b518f20a093178b7f4c00a514526610447

* GitLab Pipelines: https://gitlab.com/dsynkd/cs371p-allocator/pipelines

* Estimated completion time: 15 hours

* Actual completion time: 20 hours

* Comments: None
