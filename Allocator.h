#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <cmath> // invalid_argument
#include <iostream> // invalid_argument
#include <string> // invalid_argument

using namespace std;

//#define DEBUG 1

// ---------
// Allocator
// ---------

#define SI sizeof(int)

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;
    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;
    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;
    using       reference =       value_type&;
    using const_reference = const value_type&;

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * <your documentation>
     */
    bool valid () const {
        // <your code>
        // <use iterators>
        return true;
    }

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            // <your code>
            return *lhs == *rhs;
        }                                           // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            return *_p;
        }

        // ----------
        // operator *
        // ----------

        iterator& operator + (int a) {
            // <your code>
            _p = (int*)(((char*)_p) + a);

#if DEBUG
            cout << "+" << endl;
#endif
            return *this;
        }

        iterator& next (int a) {
#if DEBUG
            //cout << "next: " << (unsigned long)_p << " -> ";
#endif

            if(a == 0)
                a = SI;
            _p = (int*)(((char*)_p) + a);

#if DEBUG
            //cout << (unsigned long)_p << endl;
#endif

            return *this;
        }

        iterator& prev (int a) {
#if DEBUG
            //cout << "prev: " << (unsigned long)_p << " -> ";
#endif

            if(a == 0)
                a = SI;
            _p = (int*)(((char*)_p) - a);

#if DEBUG
            //cout << (unsigned long)_p << endl;
#endif

            return *this;
        }


        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
#if DEBUG
            //cout << "++: " << (unsigned long)_p << " -> ";
#endif

            _p = (int*)(((char*)_p) + 2*SI + abs(*_p));

#if DEBUG
            //cout << (unsigned long)_p << endl;
#endif

            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
#if DEBUG
            cout << "--: " << (unsigned long)_p << " -> ";
#endif

            this->prev(SI);
            _p = (int*)(((char*)_p) - SI - abs(*_p));

#if DEBUG
            cout << (unsigned long)_p << endl;
#endif

            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            // <your code>
            return *lhs == *rhs;
        }                                                       // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            // <your code>
            return *_p;
        }            // replace!

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            // <your code>
            _p += abs(*_p) + 2*SI;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            // <your code>
            _p -= abs(*_p) + 2*SI;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * SI)
     */
    my_allocator () {
        if(N < sizeof(T) + 2*SI)
            throw bad_alloc();
        int si = SI;
        int sentinel = N - 2*si;
        *reinterpret_cast<int*>(a) = sentinel;
        *reinterpret_cast<int*>(&a[N - si]) = sentinel;

#if DEBUG
        cout << "== my_allocator constructor ==" << endl;
        cout << (unsigned long)a << " -> " << sentinel << endl;
        cout << (unsigned long)&a[N - si] << " -> " << sentinel << endl;
#endif

        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * SI)
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type n) {
        int bytes_allocate = (n * sizeof(T) + sizeof(T));

        auto i = this->begin();
        auto e = this->end();
        int block_size_required = n * sizeof(T);
        while(*i < block_size_required && i != e)
            ++i;
        if(i == e)
            throw bad_alloc();

        int busy_sentinel = -1 * n * sizeof(T);
        int free_sentinel = *i + busy_sentinel - 2*SI;
        if(free_sentinel == 0)
            busy_sentinel -= sizeof(T);

        *i = busy_sentinel;
        i.next((abs(busy_sentinel) + SI));
        *i = busy_sentinel;
        i.next(SI);

        if(free_sentinel > 0) {
            *i = free_sentinel;
            i.next(free_sentinel + SI);
            *i = free_sentinel;
        }

        i.prev(*i);
        pointer r = reinterpret_cast<T*>(&(*i));
        i.prev(SI);
        assert(*i == free_sentinel);
        return r;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * <your documentation>
     */
    void deallocate (pointer p, size_type n) {
        iterator i((int*)p-4);
        iterator e = this->end();

        *i *= -1;
        ++i;
        i.prev(SI);
        *i *= -1;
        i.next(SI);
        iterator ir = i;
        iterator il = --i;
        //cout << "right check " << (unsigned long)&(*ir) << " -> " << *ir << endl;
        //cout << "left check " << (unsigned long)&(*il) << " -> " << *il << endl;

        if(&(*il) != &(*this->begin())) {
            //cout << "checking left" << endl;
            int* s1 = &(*il);
            il.prev(SI);
            int* s2 = &(*il);
            if(*s1 >= 0 && *s2 >= 0) {
                int sentinel = *s1 + *s2 + 2*SI;
                //cout << "merging left: " << *s1 << " + " << *s2 << sentinel << endl;
                il.prev(*il + SI);
                *il = sentinel;
                ++il;
                il.prev(SI);
                *il = sentinel;
                il.next(SI);
            }
            else {
                il.next(SI);
            }
        }

        if(&(*ir) != &(*this->end())) {
            //cout << "checking right" << endl;
            int* s1 = &(*ir);
            ir.prev(SI);
            int* s2 = &(*ir);
            if(*s1 >= 0 && *s2 >= 0) {
                //cout << "merging right" << endl;
                int sentinel = *s1 + *s2 + 2*SI;
                ir.prev(*ir + SI);
                *ir = sentinel;
                ++ir;
                ir.prev(SI);
                *ir = sentinel;
                ir.next(SI);
            }
            else {
                ir.next(SI);
            }
        }

        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(reinterpret_cast<int*>(&a[0]));
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return iterator(reinterpret_cast<int*>(&a[0]));
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
#if DEBUG
        //cout << "end: " << (unsigned long)(&a[N]) << endl;
#endif
        return iterator(reinterpret_cast<int*>(&a[N]));
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return iterator(reinterpret_cast<int*>(&a[N-1]));
    }
};

void allocator_solve (istream& r, ostream& w);

