// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

#include "Allocator.h"

using namespace std;

// ------------
// allocator_eval
// ------------

void allocator_eval (int a, my_allocator<double, 1000>& m) {
    if(a < 0) {
        my_allocator<double, 1000>::iterator i = m.begin();
        while(a < 0 && i != m.end()) {
            while(*i >= 0)
                ++i;
            ++a;
            if(a < 0)
                ++i;
        }
        m.deallocate((double*)(&(*i)+4), 1);
    }
    else
        double* tmp = m.allocate(a);
}

// -------------
// allocator_print
// -------------

void allocator_print (ostream& w, my_allocator<double, 1000>& m) {
    auto t = m.begin();
    auto e = m.end();
    while(t != e) {
        w << *t;
        ++t;
        if(t != e)
            w << " ";
    }
    w << endl;
}

// -------------
// allocator_solve
// -------------

void allocator_solve (istream& r, ostream& w) {
    string line;
    getline(r, line);
    int c = stoi(line, nullptr);
    getline(r, line);
    while(c-->0) {
        my_allocator<double, 1000> m;
        auto i = m.begin();
        getline(r, line);
        while(!line.empty()) {
            int a = stoi(line, nullptr);
            allocator_eval(a, m);
            getline(r, line);
        }
        allocator_print(w, m);
    }
}
