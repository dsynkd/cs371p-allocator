// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.h"

TEST(test1, read) {
    ASSERT_EQ(1,   1);
}

TEST(test2, read) {
    ASSERT_EQ(1,   1);
}

TEST(test3, read) {
    ASSERT_EQ(1,   1);
}

TEST(test4, read) {
    ASSERT_EQ(1,   1);
}

TEST(test5, read) {
    ASSERT_EQ(1,   1);
}

TEST(test6, read) {
    ASSERT_EQ(1,   1);
}

TEST(test7, read) {
    ASSERT_EQ(1,   1);
}

TEST(test8, read) {
    ASSERT_EQ(1,   1);
}

TEST(test9, read) {
    ASSERT_EQ(1,   1);
}

TEST(test10, read) {
    ASSERT_EQ(1,   1);
}

